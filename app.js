/**
 * Created by admin on 2015-09-08.
 */
'use strict';

var app = angular.module('motosmarty-landing', [
    'ui.router',
    'duScroll',
    'sticky',
    'ngScrollSpy',
    'uiGmapgoogle-maps',
    'ngFx',
    'angulartics',
    'angulartics.google.analytics'
]).value('duScrollOffset', 80)
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider.state('site', {
            abstract: true
        });
        $stateProvider
            .state('home', {
                parent: 'site',
                url   : '/',
                views : {
                    'content@': {
                        templateUrl: 'partials/main.html',
                        controller : 'MainController'
                    }
                }
            })
            .state('features', {
                parent: 'site',
                url   : '/features/:section',
                views : {
                    'content@': {
                        templateUrl: 'partials/features.html',
                        controller : 'FeaturesController'
                    }
                }
            }).state('how-it-works', {
                parent: 'site',
                url   : '/how-it-works',
                views : {
                    'content@': {
                        templateUrl: 'partials/how-it-works.html',
                        controller : 'HowItWorksController'
                    }
                }
            }).state('enterprises', {
                parent: 'site',
                url   : '/enterprises',
                views : {
                    'content@': {
                        templateUrl: 'partials/enterprises.html',
                        controller : 'EnterprisesController'
                    }
                }
            }).state('about', {
                parent: 'site',
                url   : '/about',
                views : {
                    'content@': {
                        templateUrl: 'partials/about.html',
                        controller : 'AboutController'
                    }
                }
            }).state('contact', {
                parent: 'site',
                url   : '/contact',
                views : {
                    'content@': {
                        templateUrl: 'partials/contact.html',
                        controller : 'ContactController'
                    }
                }
            });
    }])
    .config(['uiGmapGoogleMapApiProvider', function (GoogleMapApiProviders) {
        GoogleMapApiProviders.configure({
            v        : '3.20', //defaults to latest 3.X anyhow
            libraries: ''
        });
    }])
    .controller('MainController', ['$scope', '$document', '$window', function ($scope, $document, $window) {
        $scope.ui                  = {
            requestDialog: false
        };
        $scope.scrollToSection = function (id) {
            $document.scrollToElement(angular.element('#'+id), 200, 400);
        };
        $scope.toggleRequestDialog = function () {
            $scope.ui.requestDialog = !$scope.ui.requestDialog;
        };
        $scope.msg = {
            fname: null,
            lname: null,
            company: null,
            email: null
        };
        $scope.sendMsg = function () {
            console.log('send');
            $http.post('/request-a-demo', {
                name: $scope.msg.fname + ' ' + $scope.msg.lname,
                email: $scope.msg.email,
                company: $scope.msg.company
            }).then(function (resp) {
                console.log(resp);
            }, function (err) {
                console.log(err);
            });
            $scope.toggleRequestDialog();
        };
        $document.scrollTop(0);

    }])
    .controller('HowItWorksController', ['$document', '$timeout', function ($document, $timeout) {
        $document.scrollTop(0);

        var backgrounds = $('.hiw-carousel-backgrounds').children();
        angular.forEach(backgrounds, function (elem, index) {
            if(index > 0){
                $(elem).fadeOut();
            }
        });
        var currentElem = $('#hiw-carousel>.carousel-inner').children().first();
        $('#hiw-carousel').on('slide.bs.carousel', function (evt) {
            var duration = 700;
            var prev = currentElem.index();
            var current = $(evt.relatedTarget).index();
            $(backgrounds[prev]).fadeOut(duration);
            $(backgrounds[current]).fadeIn(duration);
            currentElem = $(evt.relatedTarget);
        });
    }])
    .controller('EnterprisesController', ['$scope', '$window', '$document', function ($scope, $window, $document) {
        $document.scrollTop(0);
        function menuHeight() {
            return $window.innerWidth <= 991 ? 100 : 60;
        }

        $scope.ui = {
            isSm      : ($window.innerWidth <= 991),
            menuHeight: menuHeight()
        };
        console.log($window.offsetWidth);
        $(window).on('resize.doResize', function () {
            console.log($window.innerWidth);
            $scope.$apply(function () {
                console.log(menuHeight());
                $scope.ui.isSm       = $window.innerWidth <= 991;
                $scope.ui.menuHeight = menuHeight();
            })
        })

    }])
    .controller('ContactController', ['$scope', 'uiGmapGoogleMapApi', '$document', '$http',
        function ($scope, uiGmapGoogleMapApi, $document, $http) {
            $document.scrollTop(0);
            $scope.map = {
                center : { latitude: 50.8791704, longitude: 4.7162697 },
                zoom   : 16,
                options: {
                    styles          : [
                        {
                            "featureType": "landscape",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 65
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 51
                                },
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 30
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 40
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "stylers"    : [
                                {
                                    "saturation": -100
                                },
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.province",
                            "stylers"    : [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels",
                            "stylers"    : [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "lightness": -25
                                },
                                {
                                    "saturation": -100
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers"    : [
                                {
                                    "hue": "#ffff00"
                                },
                                {
                                    "lightness": -25
                                },
                                {
                                    "saturation": -97
                                }
                            ]
                        }
                    ],
                    disableDefaultUI: true,
                }
            };
            $scope.email = {
                email: null,
                name: null,
                message: null,
                sent: false,
                send: function () {
                    $http.post('/contact', {
                        name: $scope.email.name,
                        email: $scope.email.email,
                        message: $scope.email.message
                    }).then(function (resp) {
                        console.log(resp);
                        $scope.email.sent = true;
                    }, function (err) {
                        console.log(err);
                    })
                }
            }
        }])
    .controller('AboutController', ['$scope', '$document', function ($scope, $document) {
        $scope.members = [
            {
                number     : 1,
                name       : 'dr. mateusz maj',
                position   : 'co-founder',
                description: "Actuary, double PhD - math and economics. Loves new challenges and swims against the tide. Devoted hiker.",
                tt: 'https://twitter.com/mat_maj',
                lin: 'https://www.linkedin.com/in/matmaj'
            },
            {
                number     : 2,
                name       : 'dr. maciej myslinski',
                position   : 'co-founder',
                description: "PhD and Full Stack wireless Engineer. Technology fan, family guy and devoted skier.",
                tt: 'https://twitter.com/macmys',
                lin: 'https://www.linkedin.com/in/macmys'
            },
            {
                number     : 3,
                name       : 'rudradeb mitra',
                position   : 'founding team member',
                description: "Software Engineer turned Entrepreneur. Loves solving big problems.",
                lin: 'https://www.linkedin.com/in/mitrar'
            },
            {
                number     : 4,
                name       : 'alberto gruarin',
                position   : 'ux/ui interaction designer',
                description: "Usability expert. Transforms data into user-friendly products.",
                lin: 'https://www.linkedin.com/in/albertogruarin'
            },
            {
                number     : 5,
                name       : 'Radek Czajka',
                position   : 'Lead developer',
                description: "",
                tt: 'https://twitter.com/radekczajka',
                lin: 'https://www.linkedin.com/in/rczajka'
            },
            {
                number     : 6,
                name       : 'prof. Geert Wets',
                position   : 'Scientific advisor, IMOB',
                description: ""
            },
            {
                number     : 7,
                name       : 'dr. Yongjun Shen',
                position   : 'Transportation & Mobility expert, IMOB',
                description: ""
            },
            {
                number     : 8,
                name       : 'dr. Kris Brijs',
                position   : 'Driving Psychology, IMOB',
                description: ""
            },
			{
                number     : 9,
                name       : 'Erwin Heyse',
                position   : 'VP Business development',
                description: "",
                lin: 'https://be.linkedin.com/pub/erwin-heyse/0/2a/b34'
            },
			{
                number     : 10,
                name       : 'Ansar Yasar',
                position   : 'Business development, IMOB',
                description: ""
            },
			{
                number     : 11,
                name       : 'Samira Abid',
                position   : ' Marketing & community management',
                description: "She likes taking photographs, reading and learning new languages",
                tt: 'https://twitter.com/samira1001'
            },
			{
                number     : 12,
                name       : 'Geraldine Van Cauwenbergh',
                position   : 'Marketing & community management',
                description: "Team player with thirst for knowledge",
                lin: 'https://www.linkedin.com/pub/g%C3%A9raldine-van-cauwenbergh/40/8b8/400',
                tt: 'https://twitter.com/gevc5'
            },
			{
                number     : 13,
                name       : 'Sileshi Tessema',
                position   : 'Java developer',
                description: ""
            },
			{
                number     : 14,
                name       : 'Victor Gradinescu',
                position   : 'Senior Android developer',
                description: "He has built several top apps (consumer & enterprise oriented) and has a long background as a full-stack Java developer.",
				lin: 'https://ro.linkedin.com/in/victorradugradinescu'
            },
			{
                number     : 15,
                name       : 'Wojtek Rynkowski',
                position   : 'Java developer',
                description: ""
            },
			{
                number     : 16,
                name       : 'Michal Fraczkiewicz',
                position   : 'Front-end developer',
                description: ""
            }
        ];
        $document.scrollTop(0);
    }])
    .controller('FeaturesController', ['$scope', '$document', '$stateParams', function ($scope, $document, $stateParams) {
        var analytics = angular.element('#features-tabs-analytics'),
            feedback  = angular.element('#features-tabs-feedback'),
            community = angular.element('#features-tabs-community'),
            rewards   = angular.element('#features-tabs-rewards');

        switch ($stateParams.section) {
            case 'analytics':
                $document.scrollTo(analytics, 10, 0);
                break;
            case 'feedback':
                $document.scrollTo(feedback, 10, 0);
                break;
            case 'community':
                $document.scrollTo(community, 10, 0);
                break;
            case 'rewards':
                $document.scrollTo(rewards, 10, 0);
                break;
            default:
                $document.scrollTop(0);
        }
    }])
    .controller('TopNavController', ['$scope', '$rootScope', '$state', '$document',
        function ($scope, $rootScope, $state, $document) {
            $scope.currentState     = $state.current.name;
            $scope.ui               = {
                mobileMenu: false,
                isTop     : true
            };
            $rootScope.$on('$stateChangeSuccess', function () {
                $scope.currentState = $state.current.name;
            });
            $scope.toggleMobileMenu = function () {
                $scope.ui.mobileMenu = !$scope.ui.mobileMenu;
            };
//            var menuLg              = angular.element(document.getElementById('desktop-brand-menu'));
//            menuLg.on('scroll', function () {
//                console.log('scrolled to: ', menuLg.scrollLeft(), menuLg.scrollTop());
//            });
            $document.on('scroll', function () {
                $scope.$apply(function () {
                    $scope.ui.isTop = ($document.scrollTop() === 0);
                });
                console.log($scope.ui.isTop);
            });
            $scope.isTop            = function () {
                console.log($document.scrollTop());
                return $document.scrollTop() === 0;
            };
            $scope.logoClick        = function () {
                console.log($scope.ui);
                console.log($document.scrollTop());
            }
        }])
    .directive('requestADemo', function () {
        return {
            restrict   : 'E',
            templateUrl: 'partials/request-a-demo.template.html',
            controller : function ($scope, $http) {
                $scope.ui           = {
                    requestDialog: false
                };
                $scope.toggleDialog = function () {
                    $scope.ui.requestDialog = !$scope.ui.requestDialog;
                };
                $scope.msg = {
                    fname: null,
                    lname: null,
                    company: null,
                    email: null
                };
                $scope.sendMsg = function () {
                    console.log('send');
                    $http.post('/request-a-demo', {
                        name: $scope.msg.fname + ' ' + $scope.msg.lname,
                        email: $scope.msg.email,
                        company: $scope.msg.company
                    }).then(function (resp) {
                        console.log(resp);
                    }, function (err) {
                        console.log(err);
                    });
                    $scope.toggleDialog();
                }
            }
        };
    }).directive('teamMember', function () {
        return {
            restrict   : 'A',
            templateUrl: 'partials/team-member.directive.template.html',
            scope      : {
                "member": '='
            },
            link       : function (scope, element, attributes) {
                scope.showCaption = false;
            }
        };
    });
